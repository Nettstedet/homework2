"use strict"
//Упражнение 1

let p = new Promise(function (resolve, reject) {
    console.log("Создание промиса");
    resolve();
});

p.then(function () {
    console.log("Обработка промиса");
});

console.log("Конец скрипта");

setTimeout(function timeout() {
    console.log("Таймаут");
}, 0);

//let p = new Promise  появится первым,потому что это скрипт котоый попадает в стек и сразу выполняется
//console.log("Конец скрипта") появится вторым,т.к попадает в стек и выполнеятеся следом 
//p.then попадает из стека в очередь микрозадач и выполняется после очереди макрозадач 
//setTimeout Появится последний,потому что это макрозадача котороя выполнится после выполнения микрозадаи


//Упражнение 2 


let user = {
    name: 'Nikita',
    email: ' example@mail.ru'
}
fetch('https://reqres.in/api/users',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(user)
    
}).then(console.log(user))


//Упражнение 3 
let i = 0;

let start = Date.now();

function count() {

    do{
        i++
    }while (i % 1e6 !=0)

    if(i == 1e9){
    alert("Done in " + (Date.now() - start) + 'ms');
}else {
    setTimeout(count)}
}

count();

