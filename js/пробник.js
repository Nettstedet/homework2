let form = document.getElementById('form');
let nameID = document.getElementById('name');
let mailID = document.getElementById('E-mail');
let rigths = document.getElementById('rigths');
let check = document.getElementById('check');
let errorText = document.getElementById('errorText');
let errorMail = document.getElementById('errorMail');
let ERROR = "request__input__error";
let ChekErr = "request__rights-ERROR";

function callback(validForm) {
    validForm.preventDefault();
    nameID.classList.remove(ERROR);
    mailID.classList.remove(ERROR);
    rigths.classList.remove(ChekErr);
    if (nameID.value.length == 0) {
        nameID.classList.add(ERROR);
        errorText.style.opacity = "1";
        console.error('Поле обязательно для заполнения')
       return;
    }
    if (nameID.value.length < 8) {
        nameID.classList.add(ERROR);
        console.error('Имя должно содержать как минимум 8 символов')
        return;
    }

    if (!mailID.value.includes("@") ) {
        mailID.classList.add(ERROR);
        errorMail.style.opacity = "1";
        console.error('Введите валидное значение');
       return;
    }
    if (mailID.value.length == 0) {
        mailID.classList.add(ERROR);
        errorMail.style.opacity = "1";
        console.error('Поле обязательно для заполнения');
        return;
    }

    if (check.checked === false) {
        rigths.classList.add(ChekErr);
        console.error("Поле обязательно для заполнения")
        return;
    }
    
    alert('Форма успешно отправлена')
        console.log({
            name: nameID.value,
            email: mailID.value
        }) 
    
    nameID.value = ''; //очищаем поле после отправки
    mailID.value = '';

}

form.addEventListener("submit", callback);