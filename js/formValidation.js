'use strict';

let form = document.getElementById('form');
let nameID = document.getElementById('name');
let mailID = document.getElementById('Email');
let rigths = document.getElementById('rigths');
let check = document.getElementById('check');
let errorText = document.getElementById('errorText');
let errorMail = document.getElementById('errorMail');
let NameValid = document.getElementById('nameNoValid');
let MailValid = document.getElementById('mailNoValid');
let ERROR = "request__input__error";
let ChekErr = "request__rights-ERROR";
const nameIdLocal = document.getElementById('name');
const mailIdLocal = document.getElementById('Email');
const FormObject = "FormObject";

function callback(validForm) {
    validForm.preventDefault();
    nameID.classList.remove(ERROR);
    mailID.classList.remove(ERROR);
    rigths.classList.remove(ChekErr);
    if (nameID.value.length == 0) {
        nameID.classList.add(ERROR);
        errorText.style.opacity = "1";
        errorText.textContent = "Поле обязательно для заполнения";
        console.error('Поле обязательно для заполнения')
        return;
    } else {
        errorText.style.opacity = "0";
    }
    if (nameID.value.length < 8) {
        errorText.style.opacity = "1";
        errorText.textContent = "Имя должно содержать как минимум 8 символов";
        console.error('Имя должно содержать как минимум 8 символов')
        return;
    }
    if (mailID.value.length == 0) {
        mailID.classList.add(ERROR);
        MailValid.style.opacity = "1";
        console.error('Поле обязательно для заполнения');
        return;
    } else {
        MailValid.style.opacity = "0";
    }

    if (!mailID.value.includes("@")) {
        mailID.classList.add(ERROR);
        errorMail.style.opacity = "1";
        errorMail.textContent = "Введите валидное значение";
        console.error('Введите валидное значение');
        return;
    } else {
        errorMail.style.opacity = "0";
    }


    if (check.checked === false) {
        rigths.classList.add(ChekErr);
        console.error("Поле обязательно для заполнения")
        return;
    }

    alert('Форма успешно отправлена')
    console.log({
        name: nameID.value,
        email: mailID.value
    })

    nameID.value = ''; //очищаем поле после отправки
    mailID.value = '';
    localStorage.removeItem(FormObject)

}

form.addEventListener("submit", callback);



[nameIdLocal, mailIdLocal].forEach((input) =>
    input.addEventListener("input", () => {
        const Savename = nameIdLocal.value;
        const Savemail = mailIdLocal.value;

        localStorage.setItem(FormObject, JSON.stringify({
            name: Savename,
            mail: Savemail,
        }));
    })
);
const getFormValueStirng = localStorage.getItem(FormObject);
if (getFormValueStirng !== null) {
     getFormValue = JSON.parse(getFormValueStirng);
    nameIdLocal.value = getFormValue.name;
    mailIdLocal.value = getFormValue.mail;
}