let nameID = document.getElementById('name');
let mailID = document.getElementById('Email');
let rigths = document.getElementById('rigths');
let check = document.getElementById('check');
let errorText = document.getElementById('errorText');
let errorMail = document.getElementById('errorMail');
let NameValid = document.getElementById('nameNoValid');
let MailValid = document.getElementById('mailNoValid')
let ERROR = "request__input__error";
let ChekErr = "request__rights-ERROR";



class FormOOP {
    constructor(form) {
        this.form = form;
        form.addEventListener("submit", this.sendForm.bind(this));

    }

    validateForm() {
        let errorCounter = 0
        if (nameID.value.length == 0) {
            nameID.classList.add(ERROR);
            errorText.style.opacity = "1";
            errorText.textContent = "Поле обязательно для заполнения";
            console.error('Поле обязательно для заполнения');
            errorCounter++;

        } else {
            errorText.style.opacity = "0";
        }
        if (nameID.value.length < 8) {
            errorText.style.opacity = "1";
            errorText.textContent = "Имя должно содержать как минимум 8 символов";
            console.error('Имя должно содержать как минимум 8 символов');
            errorCounter++;

        }
        if (mailID.value.length == 0) {
            mailID.classList.add(ERROR);
            MailValid.style.opacity = "1";
            console.error('Поле обязательно для заполнения');
            errorCounter++;

        } else {
            MailValid.style.opacity = "0";
        }

        if (!mailID.value.includes("@")) {
            mailID.classList.add(ERROR);
            errorMail.style.opacity = "1";
            errorMail.textContent = "Введите валидное значение";
            console.error('Введите валидное значение');
            errorCounter++;

        } else {
            errorMail.style.opacity = "0";
        }


        if (check.checked === false) {
            rigths.classList.add(ChekErr);
            console.error("Поле обязательно для заполнения");
            errorCounter++;

        }

        return errorCounter === 0;
    }

    sendForm(event) {
        event.preventDefault();
        this.sendFormWidthValidation();
    }
    sendFormWidthValidation() { 
        nameID.classList.remove(ERROR);
        mailID.classList.remove(ERROR);
        rigths.classList.remove(ChekErr);
        const isValid = this.validateForm();

        if (isValid) {
            alert('Форма успешно отправлена')
            console.log({
                name: nameID.value,
                email: mailID.value
            })

            nameID.value = ''; //очищаем поле после отправки
            mailID.value = '';

        }
        console.log(isValid)
    }
    
}


const FormObject = "FormObject";

class FormOOPSave extends FormOOP {
    constructor(form) {
        super(form);
        this.watchInput();

        const getFormValueStirng = localStorage.getItem(FormObject);
        if (getFormValueStirng !== null) {
           const nameInput = form.name;
            const emailInput = form.Email;
           const  getFormValue = JSON.parse(getFormValueStirng);
           nameInput.value = getFormValue.name;
           emailInput.value = getFormValue.Email;

        }
    }

    watchInput() {
        const nameInput = form.name;
        const emailInput = form.Email;

        [nameInput, emailInput].forEach((input) =>
            input.addEventListener("input", () => {
                const Savename = nameInput.value;
                const Savemail = emailInput.value;

                localStorage.setItem(FormObject, JSON.stringify({
                    name: Savename,
                    mail: Savemail,
                }));
            })
        );
    }

    sendForm(event) { 
        event.preventDefault();
        this.sendFormWidthValidation();
        localStorage.removeItem(FormObject);
    }
}


let form = document.getElementById('form');
const feedbackForm = new FormOOPSave(form);