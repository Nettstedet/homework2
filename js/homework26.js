/*
//Вычисление факториала с помощью рекурсии
function factorial(n) {
    //условие выхода из рекурсии
if(n===1 ) return 1;
return n*factorial(n-1);

}
 
let to = new Promise (function(resolve){
setTimeout(function() {
    resolve("ok");} , 3000);
});
to 
.then(function(){
    console.log("Обещание выполнено");
})
*/
let time = parseInt(prompt(`Введите число`))
let promise = new Promise((resolve , reject) => {
   let setIntervalId = setInterval(() => {
    if(time > 0 ){
    console.log("Осталось"  + time);
    time = time - 1;
    if ( time == 0 ){
     clearInterval(setIntervalId)
     resolve(`Все отлично`);
    }; } else{
        reject(`ЧТо-то не так`)
    }
    },1000)
   
});

promise
.then(function(){
    console.log(`Время вышло`)
})

.catch(function(){
    console.log(`Вы ввели неверное значение,ОШИБКА!`)
})


//Упражнение 2 
let fetchPromise = fetch("https://reqres.in/api/users?per_page=12");

let jsonPromise = fetchPromise.then((res) => {
    return res.json();
});

jsonPromise.then((json) => {
     let users = json.data;
     let str  = `Получили пользователей: ${users.length} \n`;
     let usersStr = users.map((user) => {
        return `- ${user.first_name}   ${user.last_name} (${user.email})` 
     })
     console.log(str + usersStr.join(`\n`));
})